Source: ruby-rack-oauth2
Section: ruby
Priority: optional
Maintainer: Debian Ruby Extras Maintainers <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Balasankar C <balasankarc@autistici.org>
Build-Depends: debhelper-compat (= 12),
               gem2deb,
               rake,
               ruby-activesupport (>= 2.3),
               ruby-attr-required (>= 0.0.5),
               ruby-httpclient (>= 2.4),
               ruby-json-jwt (>= 1.9.0),
               ruby-multi-json (>= 1.3.6),
               ruby-rack (>= 1.1),
               ruby-rspec,
               ruby-rspec-its,
               ruby-webmock
Standards-Version: 4.4.0
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-rack-oauth2.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-rack-oauth2
Homepage: https://github.com/nov/rack-oauth2
Testsuite: autopkgtest-pkg-ruby
XS-Ruby-Versions: all

Package: ruby-rack-oauth2
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ruby | ruby-interpreter,
         ruby-activesupport (>= 2.3),
         ruby-attr-required (>= 0.0.5),
         ruby-httpclient (>= 2.4),
         ruby-json-jwt (>= 1.9.0),
         ruby-multi-json (>= 1.3.6),
         ruby-rack (>= 1.1),
         ${misc:Depends}
Description: Rack interface for an OAuth 2.0
 This gem provides a Rack interface for an OAuth 2.0 Server & Client Library.
 It supports both Bearer and MAC token types. Rack provides a minimal, modular,
 and adaptable interface for developing web applications in Ruby. OAuth is an
 open standard for authorization. OAuth provides client applications a 'secure
 delegated access' to server resources on behalf of a resource owner.
